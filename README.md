# From Scratch

> Version 0.5.1

## Introduction

Projet JavaScript rudimentaire pour apprendre à utiliser **Webpack**, **BabelJS**, **ESLint** et **TypeScript** à partir de rien.

### Périmètre

- [x] Webpack
- [x] BabelJS
- [x] ESLint
- [x] TypeScript

## Processus

Récupération :

```
git clone https://gitlab.com/DmnChzl/From-Scratch.git
```

Installation :

```
yarn install
```

Utilisation :

```
yarn start
```

### Notes

_Le fichier_ `tslint.js` _contient la configuration de TSLint pour le projet_

_Ce fichier a été généré à partir de la commande suivante :_ `yarn tslint --init`

## Documentations

- **BabelJS** : Un compilateur pour JavaScript ;
  - [https://babeljs.io/](https://babeljs.io/)

- **ESLint** : Utilitaire de _linting_ pour JavaScript et JSX ;
  - [https://eslint.org/](https://eslint.org/)

- **TypeScript** : Sur-ensemble typé de JavaScript ;
  - [https://www.typescriptlang.org/](https://www.typescriptlang.org/)

- **Webpack** : Module de _bundler_ statique pour les applications JavaScript ;
  - [https://webpack.js.org/](https://webpack.js.org/)

## Licence

```
"LICENCE BEERWARE" (Révision 42):
<phk@FreeBSD.ORG> a créé ce fichier. Tant que vous conservez cet avertissement,
vous pouvez faire ce que vous voulez de ce truc. Si on se rencontre un jour et
que vous pensez que ce truc vaut le coup, vous pouvez me payer une bière en
retour. Damien Chazoule
```
